'use strict';

const webpack = require('webpack');
const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

var OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');


let config = {
  	entry: {
    	app: [
			'./_devapp/js/app.js',
			'./_devapp/scss/app.scss'
    	],
	second:[
			'./_devapp/js/second.js',
			'./_devapp/scss/second.scss'		
	],

  	},
  	output: {
		path: path.resolve(__dirname, '../..'),
		filename: 'js/[name].bundle.js'
	},
	resolve: {
        extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
        
    },
	
  	module: {
		rules: [
			{
				test: /\.(js|jsx|tsx|ts)$/,
				exclude:path.resolve(__dirname, 'node_modules'),
				use: {
				loader: 'babel-loader',
				options: {
					presets: [
							'@babel/preset-env',
							'@babel/preset-react',
							'@babel/preset-typescript'
						],
						plugins : [
							["@babel/plugin-proposal-decorators", { "legacy": true }],
							'@babel/plugin-syntax-dynamic-import',
							['@babel/plugin-proposal-class-properties', { "loose": true }]
						]
					}
				},
			},
            {
                test: /\.s?[ac]ss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    { loader: 'css-loader', options: { url: false, sourceMap: true } },
                    { loader: 'sass-loader', options: { sourceMap: true } }
                ],
            },
			{
				test: /.(png|woff(2)?|eot|ttf|svg|gif)(\?[a-z0-9=\.]+)?$/,
				use: [
				{
					loader: 'file-loader',

					options: {
						name: '../css/[hash].[ext]',
						
					}
				}
				]
			},
			{
				test : /\.css$/,
				use: ['style-loader', 'css-loader', 'postcss-loader']
			}
		]
	},
	externals: {
		myApp: 'myApp',
	},
	optimization: {
		minimizer: [
		  // we specify a custom UglifyJsPlugin here to get source maps in production
		  new UglifyJsPlugin({
			cache: true,
			parallel: true,			
			uglifyOptions: {
			  compress: false,
			  ecma: 6,
			  mangle: true,
			  output: {
			  	comments: false
			   }
			},
			sourceMap: true
		  }),


		  new OptimizeCSSAssetsPlugin({
			cssProcessorPluginOptions: {
			  preset: ['default', { discardComments: { removeAll: true } }],
			}
		  })

		],
	},
  	plugins: [
		
		new webpack.DefinePlugin({
			'__DEV__' : JSON.stringify(true),
			'__API_HOST__' : JSON.stringify('http://localhost/my-app/'),
		}),
		new MiniCssExtractPlugin({
            		filename: "css/[name].css",
			chunkFilename: "css/[id].css",
        	})
	],
	  
};



module.exports = config;
